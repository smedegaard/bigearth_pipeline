# Running the pipeline

At the moment it is only possible to run the pipeline if you have credentials to access my BigQuery and Google Could Storage.



# Inbalanced Dataset

When running this pipeline you will get an inbalanced dataset with a 1:3 ratio of `has_water`:`no_water` labels. Make sure to adjust the weights when training your model, or exclude `no_water` images to get a balanced dataset.

# Setup

To install development requirements run `pip install -e .[dev]` from the root folder of the project

# Tests

Run the following in the root folder: `pytest . --color yes`
