import setuptools

PACKAGE_NAME = "SENTINEL-2-TF-RECORDS"

REQUIRED_PACKAGES = [
    "apache-beam[gcp]==2.28.0",
    "tensorflow==2.4.1",
    "tensorflow-io==0.17.0",
]

setuptools.setup(
    name=PACKAGE_NAME,
    version="0.1",
    install_requires=REQUIRED_PACKAGES,
    packages=setuptools.find_packages(),
    extras_require={"dev": ["pytest", "pytest-pep8", "pytest-cov"]},
)
