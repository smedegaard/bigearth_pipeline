from enum import Enum


class Sentinel2Bands(Enum):
    """
    'resolution' in meters
    'wave_length' in nanometers
    """

    B01 = {
        "band_name": "B01",
        "resolution": 60,
        "wave_length": 443,
        "description": "Ultra blue (Coastal and Aerosol)",
    }

    B02 = {
        "band_name": "B02",
        "resolution": 10,
        "wave_length": 490,
        "description": "Blue",
    }

    B03 = {
        "band_name": "B03",
        "resolution": 10,
        "wave_length": 560,
        "description": "Green",
    }

    B04 = {
        "band_name": "B04",
        "resolution": 10,
        "wave_length": 665,
        "description": "Red",
    }

    B05 = {
        "band_name": "B05",
        "resolution": 20,
        "wave_length": 705,
        "description": "Visible and Near Infrared (VNIR)",
    }

    B06 = {
        "band_name": "B06",
        "resolution": 20,
        "wave_length": 740,
        "description": "Visible and Near Infrared (VNIR)",
    }

    B07 = {
        "band_name": "B07",
        "resolution": 20,
        "wave_length": 783,
        "description": "Visible and Near Infrared (VNIR)",
    }

    B08 = {
        "band_name": "B08",
        "resolution": 10,
        "wave_length": 842,
        "description": "Visible and Near Infrared (VNIR)",
    }

    B08A = {
        "band_name": "B08A",
        "resolution": 20,
        "wave_length": 865,
        "description": "Visible and Near Infrared (VNIR)",
    }

    B09 = {
        "band_name": "B09",
        "resolution": 60,
        "wave_length": 940,
        "description": "Short Wave Infrared (SWIR)",
    }

    B10 = {
        "band_name": "B10",
        "resolution": 60,
        "wave_length": 1375,
        "description": "Short Wave Infrared (SWIR)",
    }

    B11 = {
        "band_name": "B11",
        "resolution": 20,
        "wave_length": 1610,
        "description": "Short Wave Infrared (SWIR)",
    }

    B12 = {
        "band_name": "B12",
        "resolution": 20,
        "wave_length": 2190,
        "description": "Short Wave Infrared (SWIR)",
    }
