"""
Functions for ingesting data
"""


def create_query_strings(split: dict, bq_table: str, dev: bool) -> dict:
    """
    Takes a dict with the percentage split between training, evaluation and test sets.
    The split is expressed as fractions of 10, as ints. So `8` is 80%, `1` is 10%.

    `has_water` will make the query select either images with or without `Water Bodies` and `Sea and ocean`
    in the labels.
    """

    def make_query(split: dict, bq_table: str, dev: bool) -> str:
        query = f"""
            SELECT
            image_name
            FROM `{bq_table}`
            WHERE
            MOD(ABS(farm_fingerprint(FORMAT_TIMESTAMP("%S%H", acquisition_date))), 10) < {split}
            {"LIMIT 100" if dev else ""}
        """.replace(
            "\n", ""
        )
        return " ".join(query.split())

    query_dict = {
        "training": make_query(split["training"], bq_table, dev),
        "evaluation": make_query(split["evaluation"], bq_table, dev),
    }

    return query_dict
