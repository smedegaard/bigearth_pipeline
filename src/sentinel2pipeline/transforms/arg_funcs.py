"Functions to transform and validate commandline arguments"

import argparse
import logging
from sentinel2pipeline.data_classes import sentinel_2_bands as s2bands


def list_sort_and_depulicate(in_list: list) -> list:
    return sorted(list(set(in_list)))


def list_has_duplicates(in_list: list) -> bool:
    "Takes a list. returns True if the list has duplicates, else False"
    duplicates = set([x for x in in_list if in_list.count(x) > 1])
    return len(duplicates) > 0


def str_to_bool(bool_str: str) -> bool:
    if isinstance(bool_str, bool):
        return bool_str
    if bool_str.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif bool_str.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


class GetSplits(argparse.Action):
    def __call__(self, parser, args, values, option_string=None):
        """
        Takes a string that should contain two comma-separated integers
        that summes up to 10. returns a dict representing the split between
        training and evaluation data.
        """
        split_values = [int(x) for x in values.split(",")]

        if len(split_values) != 2:
            raise argparse.ArgumentTypeError(
                "--splits expects two values. Got {values}"
            )
        if sum(split_values) != 10:
            raise argparse.ArgumentTypeError(
                f"""The sum of the split values must equal 10. Got {split_values}"""
            )

        splits = {"training": split_values[0], "evaluation": split_values[1]}

        setattr(args, self.dest, splits)


class ValidateBands(argparse.Action):
    def __call__(self, parser, args, values, option_string=None):
        """
        Expects a list of strings that match the band names of `Sentinel2Bands`.
        Removes duplicates, sorts and validates the strings.

        Finally sets the argument to the validated list of bands.
        """
        values = sorted(values.split(","))
        if list_has_duplicates(values):
            logging.warning(
                "/nThe --bands arguments has duplicates. The pipeline will continue with a deduplicated list of bands"
            )
        # deduplicate
        values = list_sort_and_depulicate(values)
        # validate
        valid_band_names = [
            name for (name, _v) in (s2bands.Sentinel2Bands.__members__.items())
        ]
        for v in values:
            if v not in valid_band_names:
                raise argparse.ArgumentTypeError(
                    f"""
    {v} is not a valid band name.
    The valid names are:
    {valid_band_names}
                    """
                )
        # set args
        setattr(args, self.dest, values)
