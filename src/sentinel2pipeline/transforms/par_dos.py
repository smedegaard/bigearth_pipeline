import apache_beam as beam
import tensorflow as tf

from .pipeline_funcs import image_path


class ImageToTfExample(beam.DoFn):
    "`bands` is a list of Sentinel 2 band names"

    def __init__(self, bands):
        self.bands = bands

    @staticmethod
    def _bytes_feature(value):
        return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

    @staticmethod
    def _int64_feature(value):
        """Returns an int64_list from a bool / enum / int / uint."""
        return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

    def process(self, element):
        """
        Takes a dict on the form {'image_name': str, 'has_water': int} where:
          `image_name` is a string
          `has_water` is either 0 or 1.

        Gets the bands for the given image, transforms it into a TF Example along with metadata and converts it into a TF Record.

        Args:
            element: A dict with `image_name` and `has_water`
        Yields:
            Yields one TF Record per band, with the raw image data and metadata
        """

        image_name = element["image_name"]
        label = element["has_water"]

        for band in self.bands:
            path = image_path(image_name, band)
            image_content = tf.io.read_file(path)

            """Create tf example"""
            features = tf.train.Features(
                feature={
                    "image_name": self._bytes_feature(image_name.encode()),
                    "has_water": self._int64_feature(label),
                    "band_name": self._bytes_feature(band.encode()),
                    "raw_image": self._bytes_feature(image_content.numpy()),
                }
            )
            example = tf.train.Example(features=features)

            yield example


class AttachLabel(beam.DoFn):
    "Attaches a binary lable to an image name"

    def __init__(self, lable: int):
        self.lable = lable

    def process(self, element: dict):
        element["has_water"] = self.lable
        yield element
