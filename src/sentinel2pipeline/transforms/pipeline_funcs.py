"""
Helper functions for the pipeline
"""


def to_csv(element: dict, bands: list):
    return [
        f'{image_path(element["image_name"], band)},{element["has_water"]}'
        for band in bands
    ]


def image_path(image_name, band):
    return f"gs://satellite_image_data/BigEarthNet-v1.0/{image_name}/{image_name}_{band}.tif"


def image_has_band(image_name: str, allowed_bands: list):
    "Returns True if `band` is in `allowed_bands`, else False"
    print(
        f"image_has_band() | image_name: {image_name} | allowed_bands: {allowed_bands}"
    )
    for band in allowed_bands:
        if band == "08A":
            if image_name[-3:] == band:
                return True
        else:
            if image_name[-2:] == band:
                return True
    return False
