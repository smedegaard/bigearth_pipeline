import argparse
import logging
from datetime import datetime
import os

import apache_beam as beam

from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions

import sentinel2pipeline.transforms.pipeline_funcs as funcs
import sentinel2pipeline.transforms.arg_funcs as arg_funcs
import sentinel2pipeline.transforms.par_dos as par_dos
from sentinel2pipeline.data_classes import sentinel_2_bands as s2bands
from sentinel2pipeline import ingress


def run(pipeline_args, output_dir, bands, split, development):

    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(SetupOptions).save_main_session = True

    with beam.Pipeline(options=pipeline_options) as pipeline:

        has_water_queries = ingress.create_query_strings(
            split=split,
            bq_table="footprint-300009.sentinel_2.has_water",
            dev=development,
        )
        no_water_queries = ingress.create_query_strings(
            split=split,
            bq_table="footprint-300009.sentinel_2.no_water",
            dev=development,
        )

        # get training data

        training_has_water = (
            pipeline
            | "Training query: has_water"
            >> beam.io.ReadFromBigQuery(
                query=has_water_queries["training"], use_standard_sql=True
            )
            | "Attach label has_water: 1, training "
            >> beam.ParDo(par_dos.AttachLabel(1))
        )

        training_no_water = (
            pipeline
            | "Training query: no_water"
            >> beam.io.ReadFromBigQuery(
                query=no_water_queries["training"], use_standard_sql=True
            )
            | "Attach label has_water: 0, training "
            >> beam.ParDo(par_dos.AttachLabel(0))
        )

        # get evaluation data

        evaluation_has_water = (
            pipeline
            | "Evaluation query: has_water"
            >> beam.io.ReadFromBigQuery(
                query=has_water_queries["evaluation"], use_standard_sql=True
            )
            | "Attach label has_water: 1, evaluation"
            >> beam.ParDo(par_dos.AttachLabel(1))
        )

        evaluation_no_water = (
            pipeline
            | "Evaluation query: no_water"
            >> beam.io.ReadFromBigQuery(
                query=no_water_queries["evaluation"], use_standard_sql=True
            )
            | "Attach label has_water: 0, evaluation"
            >> beam.ParDo(par_dos.AttachLabel(0))
        )

        # Merge

        all_training = (
            training_has_water,
            training_no_water,
        ) | "Merge training sets" >> beam.Flatten()

        all_evaluation = (
            evaluation_has_water,
            evaluation_no_water,
        ) | "Merge evaluation sets" >> beam.Flatten()

        # write to csv files

        is_dev = "_dev" if development else ""

        (
            all_training
            | "Evaluation To CSV: no_water"
            >> beam.FlatMap(lambda image: funcs.to_csv(image, bands))
            | "Write training data"
            >> beam.io.Write(beam.io.WriteToText(f"{output_dir}/training{is_dev}.csv"))
        )

        (
            all_evaluation
            | "Evaluation To CSV: has_water"
            >> beam.FlatMap(lambda image: funcs.to_csv(image, bands))
            | "Write evaluation data"
            >> beam.io.Write(
                beam.io.WriteToText(f"{output_dir}/evaluation{is_dev}.csv")
            )
        )

        logging.info(f"Writing TFRecords to {output_dir}")
        # write to TFRecords
        (
            all_training
            | "Convert training data to TF Examples"
            >> beam.ParDo(par_dos.ImageToTfExample(bands))
            | "SerializeProto - training" >> beam.Map(lambda x: x.SerializeToString())
            | "Write training data to TF Records"
            >> beam.io.tfrecordio.WriteToTFRecord(
                file_path_prefix=os.path.join(output_dir, "training"),
                shard_name_template="training-sentinel-2-SSSSS-of-NNNNN",
                file_name_suffix=".tfrecord",
            )
        )

        (
            all_evaluation
            | "Convert evaluation data to TF Examples"
            >> beam.ParDo(par_dos.ImageToTfExample(bands))
            | "SerializeProto - evaluation" >> beam.Map(lambda x: x.SerializeToString())
            | "Write evaluation data to TF Records"
            >> beam.io.tfrecordio.WriteToTFRecord(
                file_path_prefix=os.path.join(output_dir, "evaluation"),
                shard_name_template="evaluation-sentinel-2-SSSSS-of-NNNNN",
                file_name_suffix=".tfrecord",
            )
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--output_dir",
        required=True,
        type=str,
        dest="output_dir",
        # default="gs://bigearth_s2_satelite/tf_records",
        help="Output path to write the TF Records to.",
    )

    parser.add_argument(
        "--bands",
        required=True,
        nargs="?",
        dest="bands",
        action=arg_funcs.ValidateBands,
        help="Comma-separated names of Sentinel 2 spectral bands",
    )

    parser.add_argument(
        "--split",
        required=True,
        nargs="?",
        dest="split",
        action=arg_funcs.GetSplits,
        help="The division between training and evaluation data. Two comma separated integers that summes up to 10. Eg: 9,1 for 90% training and 10% evaluation",
    )

    parser.add_argument(
        "--dev",
        dest="development",
        type=arg_funcs.str_to_bool,
        nargs="?",
        default=True,
        help="if set to true, produce a limited devlopment dataset",
    )

    known_args, pipeline_args = parser.parse_known_args()
    job_name = f"sentinel-2-to-tf-records-{datetime.now().strftime('%y%m%d-%H%M%S')}"

    output_dir = os.path.join(known_args.output_dir, "-".join(known_args.bands))
    if known_args.development:
        output_dir = os.path.join(output_dir, "development")
    print("\n------------------------")
    print(f"JOB NAME: {job_name}")
    print(f"WRITING TO: {output_dir}")
    print("------------------------")

    run(
        pipeline_args,
        output_dir=output_dir,
        bands=known_args.bands,
        split=known_args.split,
        development=known_args.development,
    )
