import pytest
from src.sentinel2pipeline.transforms import pipeline_funcs


def test_zero_eight_A_returns_true():
    image_name = "foo_bar_08A"
    allowed_bands = ["08", "08A", "f00"]
    has_band = pipeline_funcs.image_has_band(image_name, allowed_bands)
    assert has_band is True


def test_zero_eight_A_returns_true_v2():
    image_name = "foo_bar_08A"
    allowed_bands = ["08A", "09", "10"]
    has_band = pipeline_funcs.image_has_band(image_name, allowed_bands)
    assert has_band is True


def test_ten_returns_true():
    image_name = "foo_bar_10"
    allowed_bands = ["08A", "09", "10"]
    has_band = pipeline_funcs.image_has_band(image_name, allowed_bands)
    assert has_band is True


def test_lligal_band_returns_false():
    image_name = "foo_bar_666"
    allowed_bands = ["08A", "09", "10"]
    has_band = pipeline_funcs.image_has_band(image_name, allowed_bands)
    assert has_band is False
